#!/bin/sh -x

USER=pinephone
GECOS=pinephone
UGID=1000

DEFGROUPS="tty,sudo,adm,dialout,cdrom,plugdev,audio,dip,video,i2c,render"

echo "I: creating default user $USER"
adduser --gecos $GECOS --disabled-login $USER --uid $UGID

mkdir -p /home/$USER/Music
mkdir -p /home/$USER/Pictures
mkdir -p /home/$USER/Videos
mkdir -p /home/$USER/Downloads
mkdir -p /home/$USER/Documents
chown -R $UGID:$UGID /home/$USER

usermod -a -G ${DEFGROUPS} ${USER}

echo pinephone:123456 | chpasswd
adduser pinephone sudo
